# TVGla CSS Code Test #
---

#Getting Started

1. If you don't have Node.js and NPM installed you can download them here: [https://nodejs.org/en/](https://nodejs.org/en/)
2. If you need to do a global install of gulp type ```npm install -g gulp```
3. Navigate to the project folder in Terminal and type ```npm install```.  This will install the packages needed by gulp.
4. To preview the page as you work on it, run ```gulp watch```
5. To build the final files into the build directory, run ```gulp build```