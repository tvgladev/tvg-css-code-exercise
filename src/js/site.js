function search() {
	event.preventDefault();
	var searchApi = 'http://api.themoviedb.org/3/search/movie';
	var apiKey = '51a0abce642402e7b8d43b2081302e77';
	var query = $('form :input[name="movie"]').val();
	var grid = $('#movies-grid');
	resetGrid();
	doQuery(query);

	function doQuery(query) {
		$.ajax({
			url: searchApi,
			type: 'GET',
			data: {
				api_key: apiKey,
				query: query
			}
		})
		.done(function(result) {
			renderGrid(result.results);
		})
		.fail(function(xhr, status, error) {
			console.error('Error: ' + error);
			console.error('Status: ' + status);
		});
	}

	function resetGrid() {
		grid.empty();
	}
	function renderGrid(results) {
		results.forEach(function(result) {
			addToGrid(result);
		});
	}
	function addToGrid(movie) {
		console.log(movie);
		var poster = movie.poster_path,
			title = movie.title,
			id = movie.id,
			rating = movie.vote_average,
			release = movie.release_date,
			description = movie.overview;
		var item = document.createElement('div');
		item.className = 'movie-item';
		item = $(item);
		grid.append(item);

			var _holder = document.createElement('div');
			_holder.classList.add('movie-item-poster')
			if (poster) {
				var _poster = document.createElement('img');
				_poster.src = poster ? 'http://image.tmdb.org/t/p/w154' + poster : '#';
				_poster.classList.add('movie-item-poster-img');
				_poster.alt;
				_holder.append(_poster);
			}
			item.append(_holder);
		var tooltip = document.createElement('div');
		tooltip.className = 'tooltip';
		item.append(tooltip);
		if (title) {
			var _title = document.createElement('h4');
			_title.innerHTML = title;
			_title.classList.add('movie-item-title');
			tooltip.append(_title);
		}
		if (rating) {
			var _rating = document.createElement('h5');
			_rating.innerHTML = "Rating: " + rating;
			tooltip.append(_rating);
		}
		if (release) {
			var _release = document.createElement('h5');
			_release.innerHTML = "Released: " + release;
			tooltip.append(_release);
		}
		if (description) {
			var _desc = document.createElement('p');
			_desc.innerHTML = "Released: " + description;
			tooltip.append(_desc);
		}
	}
}